package com.twuc.webApp.domain.oneToMany.bidirectionalMap;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ChildEntityRepository extends JpaRepository<ChildEntity, Long> {
}

