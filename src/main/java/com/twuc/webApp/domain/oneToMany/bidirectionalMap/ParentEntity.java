package com.twuc.webApp.domain.oneToMany.bidirectionalMap;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

// TODO
//
// 请使用双向映射定义 ParentEntity 和 ChildEntity 的 one-to-many 关系。其中 ParentEntity
// 的数据表应当为如下的结构。
//
// parent_entity
// +─────────+──────────────+──────────────────────────────+
// | Column  | Type         | Additional                   |
// +─────────+──────────────+──────────────────────────────+
// | id      | bigint       | primary key, auto_increment  |
// | name    | varchar(20)  | not null                     |
// +─────────+──────────────+──────────────────────────────+
//
// <--start-
@Entity
public class ParentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 20, nullable = false)
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parentEntity", orphanRemoval = true)
    private List<ChildEntity> children = new ArrayList<>();

    public ParentEntity() {
    }

    public ParentEntity(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<ChildEntity> getChildren() {
        return children;
    }
}
// --end->
